function joinStrings(xs, y) {
    var outlet = xs[0];

    for (var i = 1; i < xs.length; i++) {
        outlet += y + xs[i];
    }

    return outlet;
}

function getViewableDate(date) {
    return joinStrings(date.split("T")[0].split("-").reverse(), "/");
}

function generateDates(creationDate, lastUpdatedDate) {
    var creationText = getViewableDate(creationDate);
    var lastUpdatedText = getViewableDate(lastUpdatedDate);
    var outlet = "Criado em " + creationText;

    if (creationText !== lastUpdatedText) {
        outlet += ", atualizado em " + lastUpdatedText;
    }

    return outlet;
}

function generateTags(tags) {
    return "Tags: " + joinStrings(tags, ", ");
}


function listPosts(posts, filter) {
    var outlet = "";
    var foundPost = false;

    for (var i = 0; i < posts.length; i++) {
        var post = posts[i];

        if (filter(post)) {
            var url = `./post.html?link=${encodeURI(post.path)}`;
            var dates = generateDates(post.creation_date, post.last_updated_date);
            var tags = generateTags(post.tags);

            foundPost = true;

            outlet += `
                <div class="box">
                    <h3 class="title">
                        <a href="${url}" class="posts-title">
                            ${post.title}
                        </a>
                    </h3>
                    <p class="post-description">
                        ${post.description}
                    </p>
                    <p class="post-dates">
                        ${dates}
                    </p>
                    <p class="post-tags">
                        ${tags}
                    </p>
                </div>
            `;
        }
    }

    if (!foundPost) {
        outlet = `
            <div class="box">
                <h3 class="title">
                    Oops!
                </h3>
                <p>
                    Nenhum post foi encontrado com este termo.
                </p>
            </div>
        `;
    }

    document.getElementById('listing').innerHTML = outlet;
}

function generatePostFilter(query) {
    return post => {
        let conditions = [
            !query,
            post.title.toLowerCase().includes(query),
            post.description.toLowerCase().includes(query),
            joinStrings(post.tags, " ").toLowerCase().includes(query)
        ];
        return conditions.some(condition => !!condition);
    };
}

function draw(posts) {
    if (posts.error !== undefined) {
        alert(posts.error);
        return;
    }

    document.getElementById('posts').innerHTML = `
        <div class="box">
            <input class="input is-rounded" type="text" placeholder="Filtrar" id="filter"></input>
            <button class="button is-rounded" id="search-button"><i class="fas fa-search"></i></button>
        </div>
        <div id="listing">
        </div>
    `;

    var searchCallback = function() {
        listPosts(posts, generatePostFilter(document.querySelector('#filter').value.toLowerCase()));
    };

    document.querySelector('#filter').addEventListener('change', searchCallback);
    document.querySelector('#search-button').addEventListener('click', searchCallback);

    listPosts(posts, function(p) {
        return true;
    });
}

function main() {
    var blog = new GithubBlog('liberdade-organizacao/posts');
    blog.loadIndex(function(posts) {
        localStorage.setItem("blog", JSON.stringify(posts));
        draw(JSON.parse(localStorage.getItem("blog")));
    });
}
