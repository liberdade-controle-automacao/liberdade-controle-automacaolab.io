function setupLoginButton() {
    var loginButton = document.getElementById("login");
    loginButton.addEventListener("click", function() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        loginButton.innerHTML = "Logging in...";
        auth(username, password, function(authResponse) {
	    if (!!authResponse.error) {
	        loginButton.innerHTML = "Failed to authenticate user";	
		return;
	    }
   
            const authKey = authResponse["auth_key"];
	    logIn(authKey);
	    downloadNotes(function(downloadResponse) {
		if (!!downloadResponse.error) {
		    loginButton.innerHTML = "Failed to authenticate user";
		    return;
		}
		loginButton.innerHTML = "Redirecting to main page...";
		const notes = JSON.parse(downloadResponse.notes);
                importNotes(notes);
		window.location.href = "./index.html";
		return;
	    });
        });
    });
}

function setup() {
    setupLoginButton();
}

