function setup() {
    var signupButton = document.getElementById("signup");
    signupButton.addEventListener("click", function() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        var notes = JSON.stringify(exportNotes());
        signupButton.innerHTML = "Signing up...";
	// creating user
        createUser(username, password, function(userCreationResponse) {
            if (!!userCreationResponse["error"]) {
                signupButton.innerHTML = "Try again";
		return;
	    }

	    // uploading notes
            var authKey = userCreationResponse["auth_key"];
            if (!!authKey) {
                logIn(authKey);
                uploadNotes(notes, function(notesUploadResponse) {
		    if (!!notesUploadResponse["error"]) {
			alert("Failed to upload notes when creating user");
		    }
                    signupButton.innerHTML = "Redirecting to main page...";
                    window.location.href = "./index.html";
		});
            } else {
                signUpButton.innerHTML = "Try again";
	    }
        });
    });
}
