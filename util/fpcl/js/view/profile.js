const DEDICATED_SERVER_CONNECTION_HTML = `
    <label>Server Link</label>
    <input id="server-link">
    <button type="button" id="server-check-button">Check</button>
    <button type="button" id="server-link-button">Save</button>
`;

const BAAS_SERVER_CONNECTION_HTML = `
    <label>Server Link</label>
    <input id="server-link">
    <br/>
    <label>Auth Key</label>
    <input id="auth-key">
    <br/>
    <button type="button"
            id="server-check-button">
        Check
    </button>
    <button type="button"
            id="server-link-button">
        Save
    </button>
`;

function setupChangePassword() {
    // this assumes the change password section is already in the page
    document.getElementById("change-password-button").addEventListener("click", function(e) {
        const oldPassword = document.getElementById("old-password").value;
        const newPassword = document.getElementById("new-password").value;
        const newPasswordAgain = document.getElementById("new-password-again").value;

        if (newPassword !== newPasswordAgain) {
            alert("New passwords do not match!");
            return;
        }

        updatePassword(oldPassword, newPassword, function(result) {
            alert((!!result.error)? result.error : "Password updated!");
        });
    });
}

function setupLogin() {
    const loginHTML = `
        <form>
            <fieldset>
                <legend>Login</legend>
                <label for="username">Username</label>
                <input type="text" id="username" placeholder="Username"/>
                <br/>
                <label for="password">Password</label>
                <input type="password" id="password" placeholder="Password"/>
                <br/>
                <button type="button" name="button" id="login">Login</button>
            </fieldset>
        </form>
        <a href="./signup.html">Sign Up Instead</a>
    `;
    document.getElementById('password-section').innerHTML = loginHTML;
    setupLoginButton();
}

function setupLogOff() {
    const logoffButtonHTML = `
        <h1>Log Off</h1>
        <button type="button" name="button" id="logoff-button">Log Off</button>
    `;
    document.getElementById("logoff").innerHTML = logoffButtonHTML;
    document.getElementById("logoff-button").addEventListener("click", function(e) {
        dropDb();
        window.location.href = `./index.html`;
    });
}

function setupDedicatedServerConfig() {
    document.getElementById("server-form-div").innerHTML = DEDICATED_SERVER_CONNECTION_HTML;

    var serverLink = document.getElementById("server-link");
    serverLink.value = getServerUrl();

    var serverLinkButton = document.getElementById("server-link-button");
    serverLinkButton.addEventListener("click", function(e) {
        setServerUrl(serverLink.value);
	setServerKind("dedicated");
        serverLinkButton.innerHTML = "Saved!";
        setTimeout(function() {
            serverLinkButton.innerHTML = "Save";
        }, 1000);
    });

    var serverCheckButton = document.getElementById("server-check-button");
    serverCheckButton.addEventListener("click", function(e) {
        const url = serverLink.value;
        testServer(url, function(result) {
            serverCheckButton.innerHTML = ((result.ok)? "Working!" : "Not working!");
            setTimeout(function() {
                serverCheckButton.innerHTML = "Check";
            }, 1000);
	});
    });

    document.getElementById("server-kind-select").value = "dedicated";
}

function setupBaasServerConfig() {
    document.getElementById("server-form-div").innerHTML = BAAS_SERVER_CONNECTION_HTML;

    var serverLink = document.getElementById("server-link");
    serverLink.value = getServerUrl();
    document.getElementById("auth-key").value = getBaasAppAuthKey();

    var serverLinkButton = document.getElementById("server-link-button");
    serverLinkButton.addEventListener("click", function(e) {
	setServerKind("baas");
        setServerUrl(serverLink.value);
	setBaasAppAuthKey(document.getElementById("auth-key").value);
        serverLinkButton.innerHTML = "Saved!";
        setTimeout(function() {
            serverLinkButton.innerHTML = "Save";
        }, 1000);
    });

    var serverCheckButton = document.getElementById("server-check-button");
    serverCheckButton.addEventListener("click", function(e) {
        const url = serverLink.value;
        testServer(url, function(result) {
            serverCheckButton.innerHTML = ((result.ok)? "Working!" : "Not working!");
            setTimeout(function() {
                serverCheckButton.innerHTML = "Check";
            }, 1000);
	});
    });

    document.getElementById("server-kind-select").value = "baas";
}

function saveBackupCallback() {
    var saveBackupButton = document.getElementById("save-backup");
    var rawBackup = document.getElementById("backup-contents").value;
    var backup = JSON.parse(rawBackup);
    importNotes(backup);

    if (isUserLoggedIn()) {
        uploadNotes(JSON.stringify(exportNotes()), function(result) {
            saveBackupButton.innerHTML = "Saved!";
            setTimeout(function() {
                saveBackupButton.innerHTML = "Save";
            }, 1000);
        });
    } else {
        saveBackupButton.innerHTML = "Saved!";
        setTimeout(function() {
            saveBackupButton.innerHTML = "Save";
        }, 1000);
    }
}

function setup() {
    // setup server connection
    if (getServerKind() === "baas") {
        setupBaasServerConfig();
    } else {
        setupDedicatedServerConfig();
    }

    var serverKindSelect = document.getElementById("server-kind-select");
    serverKindSelect.addEventListener("change", function() {
        switch (serverKindSelect.value) {
	    case "dedicated":
	        setupDedicatedServerConfig();
	        break;
            case "baas":
	        setupBaasServerConfig();
	        break;
	}
    });

    // redirect to login page if user is not logged in
    if (isUserLoggedIn()) {
        setupChangePassword();
        setupLogOff();
    } else {
        setupLogin();
    }

    // setup theme and theme radio buttons
    setExistingTheme();
    document.getElementById(`${getCurrentTheme()}-theme-radio`).checked = true;
    document.getElementById(`nord-theme-radio`).addEventListener("click", function(e) {
        setTheme("nord");
    });
    document.getElementById(`dark-theme-radio`).addEventListener("click", function(e) {
        setTheme("dark");
    });
    document.getElementById(`auto-theme-radio`).addEventListener("click", function(e) {
        setTheme("auto");
    });

    // setup backup
    document.getElementById("backup-contents").innerHTML = JSON.stringify(exportNotes());
    document.getElementById("save-backup").addEventListener("click", saveBackupCallback);

    // setup offline mode
    var offlineCheckbox = document.getElementById("offline-checkbox"); 
    offlineCheckbox.checked = getOfflineMode();
    offlineCheckbox.addEventListener("change", function() {
        toggleOfflineMode();
    });

    // setup clean database
    document.getElementById("clean-database").addEventListener("click", function() {
        if (window.confirm("Are you sure?")) {
	    location.href = "./logoff.html";
	}
    });
}
