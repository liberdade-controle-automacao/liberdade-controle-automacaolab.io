const FPCL_API_URL = "http://fpcl.liberdade.bsb.br";
const NOTES_FILENAME = "notes.json";

/**
 * @returns server URL. defaults to FPCL_API_URL if no cookie is set for that
 */
function getServerUrl() {
    return localStorage.getItem("fpcl_url") || FPCL_API_URL;
}

/**
 * Saves server URL
 * @param url new server URL
 */
function setServerUrl(url) {
    return localStorage.setItem("fpcl_url", url);
}

/**
 * Sets the server kind
 * @param kind "dedicated" or "baas"
 */
function setServerKind(kind) {
    if ((kind !== "baas") && (kind !== "dedicated")) {
        return;
    }
    return localStorage.setItem("fpcl_server_kind", kind);
}

/**
 * @returns "dedicated" or "baas"
 */
function getServerKind() {
    return localStorage.getItem("fpcl_server_kind") || "dedicated";
}

/**
 * Checks if a given URL points to a valid FPCL server
 * @param url the tentative URL
 * @param callback function that will be called with either
 *                 {ok: true} or {error: true}
 */
function testServer(url, callback) {
    var request = new XMLHttpRequest();
    request.open("GET", `${url}/health`, true);
    request.onload = function() {
        callback({ok: true});
    };
    request.onerror = function() {
        callback({error: true});
    };
    request.send();
}

/**
 * Logs the user off
 */
function logOff() {
    localStorage.removeItem("auth_key");
}

/**
 * Stores auth key on local database
 * @param auth_key key from database
 */
function logIn(auth_key) {
    localStorage.setItem("auth_key", auth_key);
}

/**
 * @returns true if the user is logged in, false otherwise
 */
function isUserLoggedIn() {
    return !!localStorage.getItem("auth_key");
}

/******************************
 * DEDICATED SERVER FUNCTIONS *
 ******************************/

/**
 * Atempts to log the user in
 * @param username username
 * @param password password
 * @param callback function dealing with the result of the request
 */
function dedicated__auth(username, password, callback) {
    var data = {
        username: username,
        password: password
    };
    var request = new XMLHttpRequest();
    request.open("POST", `${getServerUrl()}/users/auth`, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function() {
	callback((this.status === 200)? JSON.parse(this.response) : {error: "Failed to create user"});
    };
    request.onerror = function() {
        callback({error: this});
    };
    request.send(JSON.stringify(data));
}

/**
 * Atempts to create a new user
 * @param username username
 * @param password password
 * @param notes existing notes in backup form
 * @param callback function dealing with the result of the request
 */
function dedicated__createUser(username, password, callback) {
    var data = {
        username: username,
        password: password,
        notes: ""
    };
    var request = new XMLHttpRequest();
    request.open("POST", `${getServerUrl()}/users/create`, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function() {
	callback((this.status === 200)? JSON.parse(this.response) : {error: "Failed to create user"});
    };
    request.onerror = function() {
        callback({error: this});
    };
    request.send(JSON.stringify(data));
}

/**
 * Downloads notes from database
 * @param callback in backup format
 */
function dedicated__downloadNotes(callback) {
    var auth_key = localStorage.getItem("auth_key");
    var request = new XMLHttpRequest();
    request.open("GET", `${getServerUrl()}/notes?auth_key=${auth_key}`, true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    request.onload = function() {
        callback((this.status === 200)? JSON.parse(this.response) : {error: "Failed to download notes"});
    };
    request.onerror = function() {
        callback({error: this});
    };
    request.send();
}

/**
 * Upload notes to database
 * @param notes notes in backup format
 * @callback callback result from database call
 */
function dedicated__uploadNotes(notes, callback) {
    var request = new XMLHttpRequest();
    var data = {
        auth_key: localStorage.getItem("auth_key"),
        notes: notes
    };

    request.open("POST", `${getServerUrl()}/notes`, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function() {
	callback((this.status === 200)? JSON.parse(this.response) : {error: "Failed to create user"});
    };
    request.onerror = function() {
        callback({error: "Failed to upload notes"});
    };

    request.send(JSON.stringify(data));
}

/**
 * Uses the current auth key to update the user's password
 * @param oldPassword the old password
 * @param newPassoword the old password
 * @param callback function to be called with the result of the operation
 */
function dedicated__updatePassword(oldPassword, newPassword, callback) {
    var request = new XMLHttpRequest();
    var data = {
        "auth_key": localStorage.getItem("auth_key"),
        "old_password": oldPassword,
        "new_password": newPassword
    };

    request.open("POST", `${getServerUrl()}/users/password`, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onload = function() {
        callback(JSON.parse(this.response));
    };
    request.onerror = function() {
        callback({error: "Request failed :("})
    };

    request.send(JSON.stringify(data));
}

/*************************
 * BAAS SERVER FUNCTIONS *
 *************************/

/**
 * @param authKey BaaS auth key
 */
function setBaasAppAuthKey(authKey) {
    return localStorage.setItem("baas_app_auth_key", authKey);
}

/**
 * @returns BaaS auth key
 */
function getBaasAppAuthKey() {
    return localStorage.getItem("baas_app_auth_key");
}

 /**
  * Atempts to log the user in
  * @param username username
  * @param password password
  * @param callback function dealing with the result of the request
  */
function baas__auth(username, password, callback) {
    return fetch(`${getServerUrl()}/users/login`, {
        method: "POST",
	headers: {
	    "Content-Type": "application/json"
	},
	body: JSON.stringify({
	    "app_auth_key": getBaasAppAuthKey(),
	    "email": username,
	    "password": password,
	})
    }).then((response) => {
        return response.json();
    }).then((data) => {
        return callback(data);
    }).catch((error) => {
        return callback({error: error});      
    });
}

/**
 * @returns true if the user is logged in, false otherwise
 */
function baas__createUser(username, password, callback) {
    return fetch(`${getServerUrl()}/users/signup`, {
        method: "POST",
	headers: {
	    "Content-Type": "application/json"
	},
	body: JSON.stringify({
	    "app_auth_key": getBaasAppAuthKey(),
	    "email": username,
	    "password": password,
	})
    }).then((response) => {
        return response.json();
    }).then((data) => {
        return callback(data);
    }).catch((error) => {
        return callback({error: error});      
    });
}

/**
 * Downloads notes from database
 * @param callback in backup format
 */
function baas__downloadNotes(callback) {
    return fetch(`${getServerUrl()}/users/files`, {
        method: "GET",
	headers: {
	    "Content-Type": "application/json",
	    "X-USER-AUTH-KEY": localStorage.getItem("auth_key"),
	    "X-FILENAME": NOTES_FILENAME
	}
    }).then((response) => {
        return response.json();
    }).then((data) => {
        return callback({
	    notes: data,
	    error: null
	});
    }).catch((error) => {
        return callback({error: error});      
    });
}

/**
 * Upload notes to database
 * @param notes notes in backup format
 * @callback callback result from database call
 */
function baas__uploadNotes(notes, callback) {
    return fetch(`${getServerUrl()}/users/files`, {
        method: "POST",
	headers: {
	    "Content-Type": "application/json",
	    "X-USER-AUTH-KEY": localStorage.getItem("auth_key"),
	    "X-FILENAME": NOTES_FILENAME
	},
	body: JSON.stringify(notes)
    }).then((response) => {
        return response.json();
    }).then((data) => {
        return callback(data);
    }).catch((error) => {
        return callback({error: error});      
    });
}

/**
 * Uses the current auth key to update the user's password
 * @param oldPassword the old password
 * @param newPassoword the old password
 * @param callback function to be called with the result of the operation
 */
function baas__updatePassword(oldPassword, newPassword, callback) {
    return fetch(`${getServerUrl()}/users/password`, {
        method: "POST",
	headers: {
	    "Content-Type": "application/json"
	},
	body: JSON.stringify({
	    "user_auth_key": getBaasAuthKey(),
	    "old_password": oldPassword,
	    "new_password": newPassword,
	})
    }).then((response) => {
        return response.json();
    }).then((data) => {
        return callback(data);
    }).catch((error) => {
        return callback({error: error});      
    });
}

/*****************
 * API INTERFACE *
 *****************/

/**
 * @returns true if current server kind is "baas", false otherwise
 */
function isBaasServer() {
    return "baas" === getServerKind();
}

 /**
  * Atempts to log the user in
  * @param username username
  * @param password password
  * @param callback function dealing with the result of the request
  */
function auth(username, password, callback) {
    var f = dedicated__auth;
    if (getOfflineMode()) {
        return callback({status: "offline"});
    }
    if (isBaasServer()) {
        f = baas__auth;
    }
    return f(username, password, callback);
}

/**
 * @returns true if the user is logged in, false otherwise
 */
function createUser(username, password, callback) {
    var f = dedicated__createUser;
    if (getOfflineMode()) {
        return callback({status: "offline"});
    }
    if (isBaasServer()) {
        f = baas__createUser;
    }
    return f(username, password, callback);
}

/**
 * Downloads notes from database
 * @param callback in backup format
 */
function downloadNotes(callback) {
    var f = dedicated__downloadNotes;
    if (getOfflineMode()) {
        return callback({status: "offline"});
    }
    if (isBaasServer()) {
        f = baas__downloadNotes;
    }
    return f(callback);
}

/**
 * Upload notes to database
 * @param notes notes in backup format
 * @callback callback result from database call
 */
function uploadNotes(notes, callback) {
    var f = dedicated__uploadNotes;
    if (getOfflineMode()) {
        return callback({status: "offline"});
    }
    if (isBaasServer()) {
        f = baas__uploadNotes;
    }
    return f(notes, callback);
}

/**
 * Uses the current auth key to update the user's password
 * @param oldPassword the old password
 * @param newPassoword the old password
 * @param callback function to be called with the result of the operation
 */
function updatePassword(oldPassword, newPassword, callback) {
    var f = dedicated__updatePassword;
    if (getOfflineMode()) {
        return callback({status: "offline"});
    }
    if (isBaasServer()) {
        f = baas__updatePassword;
    }
    return f(oldPassword, newPassword, callback);
}

