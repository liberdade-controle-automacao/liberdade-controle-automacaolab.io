function displayBulma(inlet) {
    let outlet = inlet.replace('<table>', '<table class="table">');
    return outlet;
}

function main() {
    document.getElementById("generate").addEventListener("click", function(e) {
        let body = "...saindo aqui";
        try {
            let xmdt = new ExtendedMarkdownTable();
            let inlet = document.getElementById("inlet").value;
            let outlet = xmdt.toMarkdown(xmdt.extend(inlet));
            let md = new Remarkable();
            let html = md.render(outlet);
            body = displayBulma(html);
        } catch (e) {
            body = e;
        }

        document.getElementById("outlet").innerHTML = body;
    });
}
